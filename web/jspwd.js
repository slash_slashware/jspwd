function loadSites(){
	var sites = _getSites();
	if (sites.length == 0){
		document.getElementById('mySites').innerHTML = 'You don\'t have any saved sites';
		return;
	}
	var html = '';
	for (var i = 0; i < sites.length; i++){
		var site = sites[i];
		var siteHtml = site.site;
		if (site.username)
			siteHtml += ' ('+site.username+')';
		if (site.isNumber)
			siteHtml += ' (Numeric)';
		siteHtml += ' <a onclick="loadSite('+i+');">Load</a>';
		siteHtml += ' - <a onclick="deleteSite('+i+');">Delete</a>';
		html += siteHtml + '<br>';
	}
	document.getElementById('mySites').innerHTML = html;
}

function _getSites(){
	var sites = localStorage.getItem('myPasswordSites');
	if (!sites)
		return [];
	return JSON.parse(sites);
}

function generate(){
	var site = document.getElementById('site').value;
	var username = document.getElementById('username').value;
	var isNumber = document.getElementById('passwordType-number').checked;
	var specialChars = document.getElementById('specialChars').checked;
	var saveSite = document.getElementById('saveSite').checked;
	if (saveSite)
		_saveSite(site, username, isNumber);
	var master = document.getElementById('master').value;
	var password = getPassword(site, username, master, isNumber, specialChars);
	document.getElementById('password').value = password;
}

function _saveSite(site, username, isNumber){
	var siteInfo = {
		site: site,
		username: username,
		isNumber: isNumber
	}
	var sites = _getSites();
	for (var i = 0; i < sites.length; i++){
		var savedSite = sites[i];
		if (savedSite.site == site && savedSite.username == username && savedSite.isNumber == isNumber)
			return;
	}
	sites.push(siteInfo);
	localStorage.setItem('myPasswordSites', JSON.stringify(sites));
	loadSites();
}

function loadSite(i){
	var sites = _getSites();
	var site = sites[i];
	document.getElementById('site').value = site.site;
	document.getElementById('username').value = site.username;
	document.getElementById('passwordType-number').checked = site.isNumber;
}

function deleteSite(i){
	var sites = _getSites();
	sites.splice(i,1);
	localStorage.setItem('myPasswordSites', JSON.stringify(sites));
	loadSites();
}

function getPassword(site, user, master, number, specialChars){
    var useed = site+' '+user+' '+master;
    var seed = getHash(useed);

    // Create RNGs
    var ran1 = new LEcuyer(seed);

    // Generate
    if (number)
    	return randomString(ran1, '0123456789', 4);
    else 
    	return randomString(ran1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", (specialChars ? 2 : 4)) +
        (specialChars ? randomString(ran1, "!#$%&/()=?", 2) : '') +
        randomString(ran1, '0123456789', 2) +
        randomString(ran1, "abcdefghijklmnopqrstuvwxyz", 18, true);
}

function randomString(rnd, set, length, scramble){
	var string = '';
	for (var nchars = 0; nchars < length; nchars++) {
        string += set.charAt(rnd.nextInt(set.length - 1));
    }
	if (scramble){
		
	}
	return string;
}

function getHash(string){
	// Generate a hash for the seed
    var s, t, iso, hex;
    iso = "";
    hex = "0123456789ABCDEF";
    for (var i = 32; i < 256; i++) {
        if (i < 127 || i >= 160) {
            // Why not "s = i.toString(16);"?  Doesn't work in Netscape 3.0
            iso += "%" + hex.charAt(i >> 4) + hex.charAt(i & 0xF);
        }
    }
    iso = unescape(iso);
    s = 0;
    for (var i = 0; i < string.length; i++) {
        t = iso.indexOf(string.charAt(i));
        if (t < 0) {
            t = 17;
        }
        s = 0x7FFFFFFF & (((s << 5) | (s >> (32 - 5))) ^ t);
    }
    return s;
}